///////////////////////////////////////////////////////////////////////////////
/// SRE
///
/// Generate a segfault -- so we can look at stack frames in gdb

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

char result[64] = "Access denied\n";

int getRandom() {
   int i = random() % 100;
   return i;
}

void segfaultSometimes() {
   int i;
   i = getRandom();

   if( i == 50 ) {
      char* badPtr = (char*) 0x100;
      *badPtr = '!';
   }
   
   if( i == 64 ) {
   	strcpy( result, "Access granted: Code (%d)\n" );
   	return ;
   }

	segfaultSometimes();
   return ;
}

int main() {
	srand(42);
   segfaultSometimes();
   
   srand(99);
   srand(rand());

	printf( result, rand() );

   return -1;
}
